package ru.t1.karimov.tm.component;

import ru.t1.karimov.tm.api.controller.ICommandController;
import ru.t1.karimov.tm.api.controller.IProjectController;
import ru.t1.karimov.tm.api.controller.IProjectTaskController;
import ru.t1.karimov.tm.api.controller.ITaskController;
import ru.t1.karimov.tm.api.repository.ICommandRepository;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.api.service.IProjectService;
import ru.t1.karimov.tm.api.service.IProjectTaskService;
import ru.t1.karimov.tm.api.service.ITaskService;
import ru.t1.karimov.tm.constant.ArgumentConst;
import ru.t1.karimov.tm.constant.CommandConst;
import ru.t1.karimov.tm.controller.CommandController;
import ru.t1.karimov.tm.controller.ProjectController;
import ru.t1.karimov.tm.controller.ProjectTaskController;
import ru.t1.karimov.tm.controller.TaskController;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.repository.CommandRepository;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.service.CommandService;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.service.ProjectTaskService;
import ru.t1.karimov.tm.service.TaskService;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public Bootstrap() {
    }

    public void start(final String[] args) throws AbstractException {
        processArguments(args);

        System.out.println("** WELCOME TASK MANAGER **");
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAIL]");
            }

        }
    }

    private void processArguments(final String[] args) throws AbstractException {
        try{
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(final String arg) throws AbstractException {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void processCommand(final String command) throws AbstractException {
        if (command == null || command.isEmpty())
            throw new CommandNotSupportedException();
        switch (command) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void initDemoData() throws AbstractException {
        projectService.add(new Project("Project_01", "Desc_01", Status.IN_PROGRESS));
        projectService.add(new Project("Project_02", "Desc_02", Status.NOT_STARTED));
        projectService.add(new Project("Project_03", "Desc_03", Status.IN_PROGRESS));
        projectService.add(new Project("Project_04", "Desc_04", Status.COMPLETED));

        taskService.create("Task_01", "Desc task 1");
        taskService.create("Task_02", "Desc task 2");
        taskService.create("Task_03", "Desc task 3");
        taskService.create("Task_04", "Desc task 4");
    }

}

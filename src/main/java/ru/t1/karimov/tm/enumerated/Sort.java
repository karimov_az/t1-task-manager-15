package ru.t1.karimov.tm.enumerated;

import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.NameComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.exception.field.SortIncorrectException;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    public static Sort toSort(final String value) throws AbstractFieldException {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        throw new SortIncorrectException();
    }

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}

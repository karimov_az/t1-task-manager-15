package ru.t1.karimov.tm.api.controller;

import ru.t1.karimov.tm.exception.AbstractException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;

}

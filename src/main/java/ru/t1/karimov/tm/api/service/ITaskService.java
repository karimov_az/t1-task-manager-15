package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.enumerated.Sort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description) throws AbstractFieldException;

    Task create(String name) throws AbstractFieldException;

    Task add(Task task) throws AbstractEntityNotFoundException;

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    void remove(Task task) throws AbstractEntityNotFoundException;

    boolean existsById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id) throws AbstractFieldException;

    Task findOneByIndex(Integer index) throws AbstractFieldException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task removeById(String id) throws AbstractFieldException;

    Task removeByIndex(Integer index) throws AbstractFieldException;

    Task changeTaskStatusById (String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex (Integer index, Status status) throws AbstractException;

    int getSize();

}

package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task) throws AbstractEntityNotFoundException;

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    Task create(String name, String description) throws AbstractFieldException;

    Task create(String name) throws AbstractFieldException;

    boolean existsById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id) throws AbstractFieldException;

    Task findOneByIndex(Integer index) throws AbstractFieldException;

    void remove(Task task) throws AbstractEntityNotFoundException;

    Task removeById(String id) throws AbstractFieldException;

    Task removeByIndex(Integer index) throws AbstractFieldException;

    int getSize();

}

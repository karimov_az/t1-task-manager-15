package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}

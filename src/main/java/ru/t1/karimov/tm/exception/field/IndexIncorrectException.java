package ru.t1.karimov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(String message) {
        super(message);
    }

}
